<?php

namespace BonchDev\LaravelTPL\Commands;

use Illuminate\Console\Command;

class ComposerJson extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'laravel-tpl:composer-json';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fill composer.json with scripts';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        if (file_exists(base_path('composer.json'))) {
            $composerConfig = json_decode(file_get_contents(base_path('composer.json')));

            if (! isset($composerConfig->scripts->test)) {
                $composerConfig->scripts->test = [
                    "@php -r \"file_exists('.env') || copy('.env.example', '.env');\"",
                    "@php artisan key:generate --ansi",
                    "@php artisan storage:link",
                    "@php artisan migrate:fresh --seed",
                    "./vendor/bin/phpunit tests/Feature/*.php",
                    "./vendor/bin/phpunit tests/Unit/*.php"
                ];
            }

            if (! isset($composerConfig->scripts->migrate)) {
                $composerConfig->scripts->migrate = [
                    "@php artisan migrate --force -n"
                ];
            }

            if (! isset($composerConfig->scripts->initialize)) {
                $composerConfig->scripts->initialize = [
                    "@php artisan migrate --force -n",
                    "@php artisan db:seed -n",
                ];
            }

            if (! isset($composerConfig->scripts->cilint)) {
                $composerConfig->scripts->cilint = [
                    "./vendor/bin/phpstan analyse --error-format=gitlab --memory-limit=1G > gl-code-quality-report.json || echo \"exit code: $?\"",
                    "./vendor/bin/phpstan analyse --error-format=json --memory-limit=1G > lint.report.json || echo \"exit code: $?\"",
                ];
            }

            $logsPermissions = "chmod -R 774 ./storage/logs && chown -R nobody:nobody ./storage";

            if (! in_array($logsPermissions, $composerConfig->scripts->{"post-root-package-install"})) {
                $composerConfig->scripts->{"post-root-package-install"}[] = $logsPermissions;
            }

            $keyGenerate = "@php artisan key:generate --ansi";

            if (! in_array($keyGenerate, $composerConfig->scripts->{"post-root-package-install"})) {
                $composerConfig->scripts->{"post-root-package-install"}[] = $keyGenerate;
            }

            $storageLink = "@php artisan storage:link";

            if (! in_array($storageLink, $composerConfig->scripts->{"post-root-package-install"})) {
                $composerConfig->scripts->{"post-root-package-install"}[] = $storageLink;
            }

            $telescopePublish = "@php artisan telescope:publish";

            if (! in_array($telescopePublish, $composerConfig->scripts->{"post-root-package-install"})) {
                $composerConfig->scripts->{"post-root-package-install"}[] = $telescopePublish;
            }

            $webTinkerInstall = "@php artisan web-tinker:install";

            if (! in_array($webTinkerInstall, $composerConfig->scripts->{"post-root-package-install"})) {
                $composerConfig->scripts->{"post-root-package-install"}[] = $webTinkerInstall;
            }

            file_put_contents(
                base_path("composer.json"),
                json_encode($composerConfig,
                    JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES) . "\n"
            );
        }
    }
}
