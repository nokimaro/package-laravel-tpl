<?php

namespace BonchDev\LaravelTPL\Commands;

use Illuminate\Database\Console\Seeds\SeedCommand as DBSeedCommand;

class SeedCommand extends DBSeedCommand
{
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seed the database with records (without wrong exit code)';

    /**
     * @return int
     */
    public function handle()
    {
        if (! $this->confirmToProceed()) {
            return 0;
        }

        return parent::handle();
    }
}
