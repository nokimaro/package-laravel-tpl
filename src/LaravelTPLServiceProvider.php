<?php

namespace BonchDev\LaravelTPL;

use BonchDev\LaravelTPL\Commands\ComposerJson;
use BonchDev\LaravelTPL\Commands\RegisterHealthCheck;
use BonchDev\LaravelTPL\Commands\SeedCommand;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\ServiceProvider;

class LaravelTPLServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/laravel-tpl/.cicd/' => base_path('/.cicd/'),
            __DIR__ . '/laravel-tpl/.gitlab-ci.yml' => base_path('.gitlab-ci.yml'),
        ], 'laravel-cicd');

        $this->publishes([
            __DIR__.'/stubs/HealthCheckServiceProvider.stub' => app_path('Providers/HealthCheckServiceProvider.php'),
        ], 'healthcheck-provider');
    }

    public function register()
    {
        $this->commands([
            ComposerJson::class,
            RegisterHealthCheck::class,
            SeedCommand::class,
        ]);
    }


}
